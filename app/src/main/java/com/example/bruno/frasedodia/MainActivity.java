package com.example.bruno.frasedodia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button buttonNewPhrase;
    private TextView textNewPhrase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonNewPhrase = findViewById(R.id.button_new_phrase);
        textNewPhrase = findViewById(R.id.text_new_phrase);

        buttonNewPhrase.setOnClickListener(new ButtonNewPhraseListener(textNewPhrase));
    }
}
